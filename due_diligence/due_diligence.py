
"""
The purpose of this code is to explore concepts for defining System Uptime & Down Time
In this particular case, system uptime is defined as: 
			sum(max(available discharge power) / nameplate) for each month.

"""  

import requests 
import pandas as pd 
import json
from utils.common_utils import *
from utils.report_utils import *  
from datetime import datetime,timedelta
from pytz import timezone
import plotly.graph_objects as go
import mysql.connector




# get your credentials 

config = get_credentials()

# get all of the site data from a report that is currently stored / run in salesforce
sf_username = config['credentials']['salesforce']['username'] # Salesforce Username
sf_password = config['credentials']['salesforce']['password'] # Salesforce Password
sf_security_token = config['credentials']['salesforce']['security_token'] # Salesforce Security Token
reportID = '00O2R000003n4QG' # Salesforce Report ID 
sf_sites = get_sf_report(sf_username, sf_password, sf_security_token, reportID) # all sites + meta data from the salesforce report

# get site meta data

username = config['credentials']['ms-app']['username'] #ms-app username
password = config['credentials']['ms-app']['password'] # ms-app password
live_sites = get_live_sites (username, password) # all sites 

# TODO: Need PM Active Data. Would prefer for this info to be saved in a table. Once we have pm installed / removed dates,
# we can query bms table. 


# Query the BMS Table 


bms_data = get_bms_data (hostname, start_date, end_date)


