import pandas as pd
from pyathena import connect
from datetime import datetime, timedelta
import mysql.connector
import plotly.graph_objects as go 
import json
import requests 
import re
import os 
import math 
import sys
from os.path import expanduser
from simple_salesforce import Salesforce
import csv


######################
##### CREDENTIALS#####
######################

# get ms-app credentials from the user 
def get_ms_credentials():

	home = expanduser("~")
	path = home+'/env/local/'
	file = path + 'ks-config.json'

	credentials = json.load(open(file,"r"))

	username = credentials['credentials']['ms-app']['username']
	password = credentials['credentials']['ms-app']['password']

	return username, password


# get salesforce credentials  

def get_sf_credentials():

	home = expanduser("~")
	path = home+'/env/local/'
	file = path + 'ks-config.json'

	credentials = json.load(open(file,"r"))

	username = credentials['credentials']['salesforce']['username']
	password = credentials['credentials']['salesforce']['password']
	token = credentials['credentials']['salesforce']['security_token']

	return username, password, token 


# Get MS-APP Token 
def get_token (username, password):

	resp = requests.post('https://prod-stem-ms-app.stem.com/v1/app/login', 
							json = {"username": f'{username}' , "password": f'{password}' },
							timeout=10,
							headers={'Content-Type': 'application/json'})

	if resp.status_code == 200: 
		token = resp.json()['token']
	elif resp.status_code == 401: 
		print('Authentication Failed')
	elif resp.status_code >= 500: 
		print('Authentication request failed status [{}], content [{}]').format(resp.status_code, resp.content)
	    
	else: 
	    print ('Authentication request failed resp [{}]').format(resp.text) 

	return token       


def get_jira_credentials():

	home = expanduser("~")
	path = home+'/env/local/'
	file = path + 'ks-config.json'

	credentials = json.load(open(file,"r"))

	username = credentials['credentials']['jira']['username']
	api_key = credentials['credentials']['jira']['api_key']
	account_id = credentials['credentials']['jira']['account_id']

	return username, api_key, account_id


def get_grafana_credentials():

	home = expanduser("~")
	path = home+'/env/local/'
	file = path + 'ks-config.json'

	credentials = json.load(open(file,"r"))

	username = credentials['credentials']['grafana']['username']
	password = credentials['credentials']['grafana']['password']


	return username, password

def get_fleet_credentials(): 

	home = expanduser("~")
	path = home+'/env/local/'
	file = path + 'ks-config.json'

	credentials = json.load(open(file,"r"))

	username = credentials['credentials']['fleet-edge']['username']
	password = credentials['credentials']['fleet-edge']['password']


	return username, password


#####################
##### META DATA #####
#####################

def get_live_sites():

	username, password = get_fleet_credentials()

	payload = {'username':username,'password':password}
	auth = requests.post('https://app.stem.com/authenticate_user/',data=payload)
	data = requests.get('https://app.stem.com/powerops/fleet_data/',cookies=auth.cookies).json()

	site_name = []
	location_code = []
	system_status = []
	salesforce_id = []
	system_id = []
	system_type = []
	system_size_kw = []
	system_size_kwh = []
	powerstore_install_datetime = []

	for i in data['data']['locations'].keys():
		location_code.append(i)
		site_name.append(data['data']['locations'][i]['topology'].get('location_name', 'None'))
		system_status.append(data['data']['locations'][i]['topology'].get('optimization_status_type', 'None'))
		salesforce_id.append(data['data']['locations'][i]['topology'].get('salesforce_id', 'None'))
		system_id.append(data['data']['locations'][i]['topology'].get('salesforce_system_id','None'))
		system_type.append(data['data']['locations'][i]['topology'].get('system_type', 'None'))
		system_size_kw.append(data['data']['locations'][i]['topology'].get('system_size_kw', 'None'))
		system_size_kwh.append(data['data']['locations'][i]['topology'].get('system_size_kwh', 'None'))
		powerstore_install_datetime.append(data['data']['locations'][i]['topology'].get('powerstore_install_datetime', 'None'))

		
	sites = pd.DataFrame({"site_name":site_name,
	                      "location_code": location_code,
	                     "system_status": system_status, 
	                     "salesforce_id": salesforce_id, 
	                     "system_id":system_id,
	                     "system_type": system_type, 
	                     "system_size_kw": system_size_kw, 
	                     "system_size_kwh" : system_size_kwh, 
	                     "powerstore_install_datetime": powerstore_install_datetime
	                     })

	live_sites = sites      
	
	return live_sites 
 

# Get Salesforce Report
def get_sf_report (report_id): 
	username, password, security_token = get_sf_credentials()

	sf = Salesforce(username = f'{username}', password = f'{password}', security_token = f'{security_token}')
	response = requests.get(f"https://stempowered.my.salesforce.com/{report_id}?view=d&snip&export=1&enc=UTF-8&xf=csv",headers = sf.headers, cookies = {'sid':sf.session_id})
	data = vars(response)['_content'].splitlines()
	fin = []
	for line in data:
		fin.append(line.decode().strip())
	fin = fin[:-7]
	myreader = csv.DictReader(fin)
	sites = pd.DataFrame(myreader)

	return sites

# get power monitor activity (install / removal )
def get_pm_activity(salesforce_id): 

	hostname = []
	sf_id = []
	install_ts = []
	removed_ts = []

	username, password = get_ms_credentials()
	token = get_token(username,password)

	headers = {'Authorization': f'Bearer {token}'}

	url = 'https://prod-site-genie.stem.com/v1/site_genie/site/load?site_salesforce_id='+salesforce_id


	with requests.get(url, headers = headers) as resp:
		data = resp.json()

	location_code =data.get('code','None')
	system_type = data['ess_attrs'].get('system_type','None')
	system_size_kw = data['ess_attrs'].get('system_size_kw','None')
	system_size_kwh = data['ess_attrs'].get('system_size_kwh','None')
	site_name = data.get('name','None')
	system_id = data.get('site_id','None')
	if len(data['gs_resource']) > 0: 

		program = data['gs_resource'][0].get('program_name','None')
	else: 
		program = 'None'
	
	df = data.get('stem_pc', [{"stem_pc":None}])

	if len(df) == 0:
		sf_id.append(salesforce_id)
		hostname.append('None')
		install_ts.append('None')
		removed_ts.append('None')
	else: 
		for j in df:

			sf_id.append(salesforce_id)
			hostname.append(j.get('hostname', 'None'))
			install_ts.append(j.get('install_ts','None'))
			removed_ts.append(j.get('removed_ts','None'))

	pm = pd.DataFrame({
		"site_name": site_name,
	"location_code": location_code, 
	"salesforce_id": sf_id,
	"system_id":system_id,
	"system_type": system_type,
	"system_size_kw": system_size_kw, 
	"system_size_kwh": system_size_kwh,
	"program_name": program,
	"hostname": hostname, 
	"install_ts": install_ts, 
	"removed_ts": removed_ts

	})   

	return pm