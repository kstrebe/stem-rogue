
# Copyright © 2020 Stem Inc. (developed by Katie Strebe - katie.strebe@stem.com)

import pandas as pd
from pyathena import connect
from datetime import datetime, timedelta
import mysql.connector
import plotly.graph_objects as go
import json
import requests
import re
import os
import math
from jira import JIRA
import sys 
from simple_salesforce import Salesforce
import csv



# get credentials 

# TODO: generalize it so that it's not hardcoded file paths... 
def get_credentials(): 


    path = '/Users/katiestrebe/env/local/ks-config.json'
    config = json.load(open(path))

    return config 

def get_aws_credentials(): 
    
    path = '/Users/katiestrebe/.aws/cli/cache/'
    os.chdir(path)
    files = [f for f in os.listdir('.') if os.path.isfile(f)]
    for f in files:
        if f.endswith('json'): 
            config = json.load(open(path+f))
            
        else: 
            print('not json')
    
    return config 


        




# get sf report 

def get_sf_report(sf_username, sf_password, sf_security_token, reportID):     
    """get_sites takes in salesforce credentials and pulls a report from the Salesforce API that includes
    all of the sites and site meta data. The output is a dataframe that includes the sites 
    in the Electrodes Profile, which HEBT the sites are associated with, the Salesforce ID, 
    system information (i.e. PowerStore Type, System Size, etc)"""


    sf = Salesforce(username = f'{sf_username}', password = f'{sf_password}', security_token = f'{sf_security_token}')
    response = requests.get(f"https://stempowered.my.salesforce.com/{reportID}?view=d&snip&export=1&enc=UTF-8&xf=csv",headers = sf.headers, cookies = {'sid':sf.session_id})
    data = vars(response)['_content'].splitlines()
    fin = []
    for line in data: 
        fin.append(line.decode().strip())
    fin = fin[:-7]
    myreader = csv.DictReader(fin)
    sites = pd.DataFrame(myreader)
    
    return sites 


# Get live sites

def get_live_sites(username, password):
    # TODO: NEED TO UPDATE WITH PROPER CREDENTIAL HANDLING

    # username = ''
    # password = ''

    payload = {'username': username, 'password': password}
    auth = requests.post('https://app.stem.com/authenticate_user/', data=payload)
    data = requests.get('https://app.stem.com/powerops/fleet_data/', cookies=auth.cookies).json()

    site_name = []
    location_code = []
    system_status = []
    salesforce_id = []
    system_type = []
    for i in data['data']['locations'].keys():
        location_code.append(i)

        try:
            site_name.append(data['data']['locations'][i]['topology']['location_name'])
        except:
            site_name.append('None')
        try:
            system_status.append(data['data']['locations'][i]['topology']['optimization_status_type'])
        except:
            system_status.append('None')

        try:
            salesforce_id.append(data['data']['locations'][i]['topology']['salesforce_id'])
        except:
            salesforce_id. append('None')

        try:
            system_type.append(data['data']['locations'][i]['topology']['system_type'])

        except:
            system_type.append('None')

    sites = pd.DataFrame({"site_name": site_name,
                          "location_code": location_code,
                          "system_status": system_status,
                          "salesforce_id": salesforce_id,
                          "system_type": system_type})

    
    return sites

# Get AWS SSO Credentials


# def get_credentials(aws_secret_access_key, aws_access_key_id):
#     # TODO - use boto3 or aws-cli to access AWS

#     # ls

#     return aws_access_key_id, aws_secret_access_key

# # Connect to athena


def connect_athena(database, location):

    creds = get_credentials()

    aws_access_key_id = creds[0]
    aws_secret_access_key = creds[1]

    if database == 'athena' and location == 'east':

        db = connect(aws_access_key_id=aws_access_key_id,
                     aws_secret_access_key=aws_secret_access_key,
                     s3_staging_dir='s3://aws-athena-query-results-874390977515-us-east-2/',
                     region_name='us-east-2')

    elif database == 'athena' and location == 'west':

        db = connect(aws_access_key_id=aws_access_key_id,
                     aws_secret_access_key=aws_secret_access_key,
                     s3_staging_dir='s3://aws-athena-query-results-874390977515-us-west-2/',
                     region_name='us-west-2')

    else:
        print('Unknown database %s  or location %s' % (database, location))

    return db

# Get MS-APP Token



def get_token(username, password):
    # TODO: NEED TO UPDATE WITH PROPER CREDENTIAL HANDLING

    resp = requests.post('https://prod-stem-ms-app.stem.com/v1/app/login',
                         json={"username": f'{username}', "password": f'{password}'},
                         timeout=10,
                         headers={'Content-Type': 'application/json'})

    if resp.status_code == 200:
        token = resp.json()['token']
    elif resp.status_code == 401:
        print('Authentication Failed')
    elif resp.status_code >= 500:
        print('Authentication request failed status [{}], content [{}]').format(resp.status_code, resp.content)

    else:
        print('Authentication request failed resp [{}]').format(resp.text)

    return token

# Get Stem Hostname

# TODO: input_source/site_genie.py?


def get_stem_host(username, password, salesforce_id, start, end):
    token = get_token(username, password)

    headers = {'Authorization': f'Bearer {token}'}

    url = 'https://prod-site-genie.stem.com/v1/site_genie/site/load?site_salesforce_id='+salesforce_id

    start = pd.to_datetime(start, utc=True)
    end = pd.to_datetime(end, utc=True)

    with requests.get(url, headers=headers) as resp:
        data = resp.json()

    stem_host = []
    sf_id = []
    install_ts = []
    removed_ts = []

    for j in range(len(data['stem_pc'])):
        sf_id.append(salesforce_id)
        stem_host.append(data['stem_pc'][j]['hostname'])
        install_ts.append(data['stem_pc'][j]['install_ts'])
        removed_ts.append(data['stem_pc'][j]['removed_ts'])

    df = pd.DataFrame({"salesforce_id": sf_id, "stem_host": stem_host, "install_ts": install_ts, "removed_ts": removed_ts})
    df['install_ts'] = pd.to_datetime(df['install_ts'], utc=True)
    df['removed_ts'] = pd.to_datetime(df['removed_ts'], utc=True)

    stem_host = df[(df['install_ts'] < start) & ((df['removed_ts'].isna()) | (df['removed_ts'] < end))]['stem_host'].iloc[0]

    return stem_host


# Get live stem hostnames

def get_live_stem_host(username, password, live_sites):
    site_name = []
    system_type = []
    hostname = []
    sf_id = []
    install_ts = []
    removed_ts = []

    for i in range(len(live_sites)):
        token = get_token(username, password)
        salesforce_id = live_sites['salesforce_id'].iloc[i]

        headers = {'Authorization': f'Bearer {token}'}

        url = 'https://prod-site-genie.stem.com/v1/site_genie/site/load?site_salesforce_id='+salesforce_id

        with requests.get(url, headers=headers) as resp:
            data = resp.json()

        for j in data['stem_pc']:

            site_name.append(live_sites['site_name'].iloc[i])
            sf_id.append(live_sites['salesforce_id'].iloc[i])
            system_type.append(live_sites['system_type'].iloc[i])

            try:
                hostname.append(j['hostname'])
            except:
                hostname.append('None')
            try:
                install_ts.append(j['install_ts'])
            except:
                install_ts.append('None')
            try:
                removed_ts.append(j['removed_ts'])
            except:
                removed_ts.append('None')

    pm = pd.DataFrame({"site_name": site_name,
                       "system_type": system_type,
                       "salesforce_id": sf_id,
                       "hostname": hostname,
                       "install_ts": install_ts,
                       "removed_ts": removed_ts
                       })
    return pm


# Get Stream Data Definition

def get_definition(ts_type):

    url = 'http://prod-stem-ts.stem.com/v1/ts/definition/pc_data/' + ts_type

    token = get_token()

    headers = {'Authorization': f'Bearer {token}'}

    with requests.get(url, headers=headers) as resp:
        data = resp.json()

    data = json.loads(json.dumps(data))

    df = pd.DataFrame()
    for i in range(len(data['props'])):
        if data['props'][i]['data_type'] == 'text':

            temp = pd.DataFrame({"name": [data['props'][i]['name']],
                                 "data_type": 'string'})
        elif data['props'][i]['data_type'] == 'date':
            temp = pd.DataFrame({"name": [data['props'][i]['name']],
                                 "data_type": 'timestamp'})

        else:

            temp = pd.DataFrame({"name": [data['props'][i]['name']],
                                 "data_type": data['props'][i]['data_type']})
        df = df.append(temp)

    # Need to get definition into a form compatible for the query

    string = ''

    for i in range(len(df)):
        if i < len(df)-1:
            string = string + '`%s` %s, ' % (df['name'].iloc[i],
                                             df['data_type'].iloc[i])
        else:
            string = string + '`%s` %s ' % (df['name'].iloc[i],
                                            df['data_type'].iloc[i])
    definitions = string

    return definitions

# Get Stream ID

def get_stream_id(ts_type, salesforce_id):

    token = get_token()

    headers = {'Authorization': f'Bearer {token}'}

    url = 'https://prod-site-genie.stem.com/v1/site_genie/site/load?site_salesforce_id='+salesforce_id

    with requests.get(url, headers=headers) as resp:
        data = resp.json()

    data = json.loads(json.dumps(data))

    data_stream = []

    for key in data['data_streams'].keys():
        test = re.search(ts_type, key)
        if test != None:
            data_stream.append(key)
    stream_id = []
    for stream in data_stream:
        stream_id.append(data['data_streams'][stream]['contributors'][0]['stream_id'])

    return stream_id

# Get Product Type

def get_product_type(location_code):

    token = get_token()

    url = 'https://prod-site-genie.stem.com/v1/site_genie/site/load?site_loc_code='+location_code

    headers = {'Authorization': f'Bearer {token}'}

    with requests.get(url, headers=headers) as resp:
        data = resp.json()

    data = json.loads(json.dumps(data))

    product_type = data['ess_attrs']['system_type']

    return product_type

# Make a timeseries table

# TODO: input_source/aws_athena.py?


def make_ts_table(database, location, ts_type, location_code, year, stream_id):

    db = connect_athena(database, location)
    definition = get_definition(ts_type)

    table_name = location_code + str(year)

    cursor = db.cursor()

    query = '''CREATE EXTERNAL TABLE `{0}`({1})
	ROW FORMAT SERDE
	'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe'
	STORED AS INPUTFORMAT
	'org.apache.hadoop.mapred.TextInputFormat'
	OUTPUTFORMAT
	'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
	LOCATION
	's3://prod.data.stem.com/prod/ts/ts_type=pc_data.{2}/stream_id={3}/year={4}/'
	TBLPROPERTIES (
	'transient_lastDdlTime'='1581454319'); '''.format(table_name, definition, ts_type, stream_id, year)

    print(query)
    try:
        cursor.execute(query)
    except:
        print('something went wrong with your query')

    cursor.close()

    return table_name

# Drop a timeseries table

# TODO: input_source/aws_athena.py?


def drop_ts_table(database, location, table_name):

    db = connect_athena(database, location)

    cursor = db.cursor()

    query = '''DROP TABLE `{0}` '''.format(table_name)

    print(query)

    cursor.execute(query)
    cursor.close()

# Query Athena

# TODO: input_source/aws_athena.py?


def query_athena(database, location, query):

    print('Connecting to Athena...')

    db = connect_athena(database, location)
    print('Executing Query... \n %s' % query)

    cursor = db.cursor()

    cursor.execute(query)

    df_temp = cursor.fetchall()

    desc = cursor.description

    cols = []
    for col in desc:
        cols.append(col[0])

    df_temp = pd.DataFrame(list(df_temp))
    df_temp.columns = cols
    print('Done Fetching Data')
    print(df_temp)

    cursor.close()

    return df_temp

# Query JIRA

# TODO: input_source/jira.py?


def jira_connect(user, apikey):
    # TODO: NEED TO UPDATE WITH PROPER CREDENTIAL HANDLING

    # user = ''
    # apikey = ''

    options = {"server": "https://stemedge.atlassian.net"}

    jira = JIRA(options, basic_auth=(user, apikey))

    return jira


def query_jira(jql, jql_fields, fields):
    jira = jira_connect()

    issues = jira.search_issues(jql,  json_result=True, maxResults=10, fields=jql_fields)

    df = pd.DataFrame()

    for b in range(math.ceil((issues['total'])/100)):

        b = b*100

        issues = jira.search_issues(jql, startAt=b, json_result=True, maxResults=100,  fields=jql_fields)

        for issue in issues['issues']:

            msg = json.loads(json.dumps(issue))

            temp = pd.DataFrame()

            for field in fields:
                if field == 'key':
                    temp[field] = [msg[field]]

                elif field == 'status':
                    temp[field] = [msg['fields']['status']['name']]
                else:

                    try:
                        temp[field] = [msg['fields'][field]]
                    except:
                        temp[field] = ['None']

            df = df.append(temp, ignore_index=True)

        return df
