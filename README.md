# stem-rogue

Metric and report calculations and utilities.

## Setup (dev and user)
- Install `src/requirements.txt` into a Python "3.6-latest" virtual environment (example using `pyenv`: `pyenv install 3.6.10 && pyenv virtualenv 3.6.10 netops_reports && pyenv activate netops_reports && pip install -U pip && pip install -r src/requirements.txt`)

## Usage

- TODO

## Versions

Python: 3.6.10
Python libraries: see [requirements.txt](./requirements.txt)
